from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from rest_framework import serializers

from .models import TaxiDriver, TaxiCompany, Vehicle, TaxiTrip, TaxiTripPoint

class TaxiDriverUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password', 'first_name', 'last_name']
        read_only_fields = ['id']
        extra_kwargs = {
            'password': {'write_only' : True},
            'first_name': {'required' : True, 'allow_blank' : False},
            'last_name': {'required' : True, 'allow_blank' : False}
        }

    def validate_password(self, value):
        password_validation.validate_password(value, self.instance)
        return value

    def create(self,validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

class RegisterAsTaxiDriverSerializer(serializers.Serializer):
    first_name = serializers.ModelField(model_field=User()._meta.get_field('first_name'), write_only=True)
    last_name = serializers.ModelField(model_field=User()._meta.get_field('last_name'), write_only=True)
    email = serializers.ModelField(model_field=User()._meta.get_field('email'), write_only=True)
    password = serializers.ModelField(model_field=User()._meta.get_field('password'), write_only=True)
    user = TaxiDriverUserSerializer(read_only=True)

    def validate(self, data):
        taxiDriverUserSerializer = TaxiDriverUserSerializer(data = {
            'email' : data.get('email'),
            'username' : data.get('email'),
            'password' : data.get('password'),
            'first_name' : data.get('first_name'),
            'last_name' : data.get('last_name')
        })
        taxiDriverUserSerializer.is_valid(raise_exception=True)
        return data

    def create(self, validated_data):
        taxiDriverUserSerializer = TaxiDriverUserSerializer(data = {
            'email' : validated_data.get('email'),
            'username' : validated_data.get('email'),
            'password' : validated_data.get('password'),
            'first_name' : validated_data.get('first_name'),
            'last_name' : validated_data.get('last_name')
        })
        # Validating again in case data has become invalid between validating and creating (E.g. User with same username registered in meantime)
        taxiDriverUserSerializer.is_valid(raise_exception=True)
        user = taxiDriverUserSerializer.save()
        try:
            taxiDriver = TaxiDriver(user=user)
            taxiDriver.save()
            return {'user': taxiDriverUserSerializer.data}
        except:
            # Delete user if something went wrong
            # TODO make transaction
            user.delete()
            raise

class TaxiCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = TaxiCompany
        fields = ['id', 'company_name', 'company_address', 'wage_per_km_driven', 'active']
        read_only_fields = ['id', 'active']

class TaxiDriverSetCompanySerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    taxi_company_id = serializers.IntegerField()

    def validate(self, data):
        try:
            taxiDriver = TaxiDriver.objects.get(user=data.get('user_id'))
        except ObjectDoesNotExist:
            raise serializers.ValidationError({'user_id': ['User is not a Taxi Driver.']})
        if taxiDriver.taxi_company:
            raise serializers.ValidationError(detail='Taxi Driver already has a Taxi Company set, changing Taxi Company is not allowed.')
        try:
            TaxiCompany.objects.get(pk=data.get('taxi_company_id'))
        except ObjectDoesNotExist:
            raise serializers.ValidationError({'taxi_company_id': ['Taxi Company does not exist.']})
        return data

    def create(self, validated_data):
        taxiDriver = TaxiDriver.objects.get(user=validated_data.get('user_id'))
        taxiCompany = TaxiCompany.objects.get(pk=validated_data.get('taxi_company_id'))
        taxiDriver.taxi_company = taxiCompany
        taxiDriver.save()
        return {'user_id': validated_data.get('user_id'), 'taxi_company_id': validated_data.get('taxi_company_id')}

class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = ['car_manufacturer', 'model', 'year_of_production', 'license_plate']

class TaxiTripSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaxiTrip
        fields = ['id', 'taxi_driver', 'start_timestamp', 'end_timestamp']
        read_only_fields = ['id']

class TaxiTripPointSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaxiTripPoint
        fields = ['taxi_trip', 'lat', 'lng', 'timestamp']