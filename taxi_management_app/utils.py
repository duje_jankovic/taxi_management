from datetime import datetime

def getFormattedTimestamp(timestamp: datetime):
    return str(timestamp.strftime("%Y-%m-%d %H:%M:%S"))