from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from . import views

app_name = 'taxi_management_app'
urlpatterns = [
    path('api/login/', obtain_auth_token, name='api/login'),
    path('api/taxi_companies/', views.ListTaxiCompaniesView.as_view(), name='api/taxi_companies/'),
    path('api/taxi_drivers/register/', views.RegisterAsTaxiDriverView.as_view(), name='api/taxi_drivers/register'),
    path('api/taxi_drivers/<int:user_id>/set_company/', views.TaxiDriverSetCompanyView.as_view(), name='api/taxi_drivers/set_company'),
    path('api/taxi_drivers/<int:user_id>/set_vehicle/', views.TaxiDriverSetVehicleView.as_view(), name='api/taxi_drivers/set_vehicle'),
    path('api/taxi_trips/start_trip/', views.TaxiTripStartView.as_view(), name='api/taxi_trips/start_trip'),
    path('api/taxi_trips/<int:trip_id>/add_point/', views.TaxiTripAddPointView.as_view(), name='api/taxi_trips/add_point'),
    path('api/taxi_trips/<int:trip_id>/end_trip/', views.TaxiTripEndView.as_view(), name='api/taxi_trips/end_trip'),
]
