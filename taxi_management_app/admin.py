from django.contrib import admin
from . models import TaxiCompany, TaxiTrip, TaxiTripPoint

class TaxiTripPointInline(admin.TabularInline):
    model = TaxiTripPoint
    extra = 0
    ordering = ('-timestamp',)

class TaxiCompanyInline(admin.TabularInline):
    model = TaxiCompany
    extra = 0


class TaxiTripAdmin(admin.ModelAdmin):
    inlines = [TaxiTripPointInline]
    list_display = ('start_timestamp', 'end_timestamp_or_default', 'taxi_driver', 'taxi_company')
    list_filter = [('taxi_driver', admin.RelatedOnlyFieldListFilter), ('taxi_driver__taxi_company', admin.RelatedOnlyFieldListFilter)]
    ordering = ('-start_timestamp',)

    @admin.display(
        ordering='end_timestamp',
        description='end timestamp',
    )
    def end_timestamp_or_default(self, obj):
        return str(obj.end_timestamp) if obj.end_timestamp else 'Not finished'

    @admin.display(
        ordering='taxi_driver__taxi_company',
        description='taxi company',
    )
    def taxi_company(self, obj):
        return obj.taxi_driver.taxi_company

    def lookup_allowed(self, key, value):
        return True

admin.site.register(TaxiCompany)
admin.site.register(TaxiTrip, TaxiTripAdmin)
