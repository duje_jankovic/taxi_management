from django.apps import AppConfig


class TaxiManagementAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'taxi_management_app'
