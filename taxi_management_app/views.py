from rest_framework import generics
from rest_framework import permissions
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView

from . serializers import *
from . models import TaxiCompany, TaxiDriver
from . permissions import DjangoModelPermissionsForListAPIView

class ListTaxiCompaniesView(generics.ListAPIView):
    """
    API Endpoint retrieve a list of all active Taxi Companies the system.

    * Requires token authentication.
    """
    permission_classes = [DjangoModelPermissionsForListAPIView]
    queryset = TaxiCompany.objects.filter(active=True)
    serializer_class = TaxiCompanySerializer

class RegisterAsTaxiDriverView(generics.CreateAPIView):
    """
    API Endpoint to create Taxi Driver user
    """
    permission_classes = [permissions.AllowAny]
    authentication_classes = []
    queryset = []
    serializer_class = RegisterAsTaxiDriverSerializer

class TaxiDriverSetCompanyView(APIView):
    """
    API Endpoint to set Taxi Company for Taxi Driver.

    * Requires token authentication.
    """
    permission_classes = [permissions.IsAuthenticated]

    def patch(self, request, user_id):
        user = request.user
        if user.id != user_id and not user.has_perm('taxi_management_app.change_taxidriver'):
            raise PermissionDenied('Not allowed to change Taxi Drivers.')
        try:
            TaxiDriver.objects.get(user=user_id)
        except ObjectDoesNotExist:
            raise NotFound('User is not a Taxi Driver.')
        taxiDriverSetCompanySerializer = TaxiDriverSetCompanySerializer(data = {'user_id' : user_id, 'taxi_company_id' : request.data.get('taxi_company_id')})
        taxiDriverSetCompanySerializer.is_valid(raise_exception=True)
        taxiDriverSetCompanySerializer.save()
        return Response(taxiDriverSetCompanySerializer.data)

class TaxiDriverSetVehicleView(APIView):
    """
    API Endpoint to set Vechicle data for Taxi Driver.

    * Requires token authentication.
    """
    permission_classes = [permissions.IsAuthenticated]

    def patch(self, request, user_id):
        user = request.user
        if user.id != user_id and not user.has_perm('taxi_management_app.change_taxidriver'):
            raise PermissionDenied('Not allowed to change Taxi Drivers.')
        try:
            taxiDriver = TaxiDriver.objects.get(user=user_id)
        except ObjectDoesNotExist:
            raise NotFound('User is not a Taxi Driver.')
        if taxiDriver.vehicle:
            if not user.has_perm('taxi_management_app.change_vehicle'):
                raise PermissionDenied('Not allowed to change Vehicles.')
            vehicleSerializer = VehicleSerializer(instance = taxiDriver.vehicle, data = request.data)
        else:
            if not user.has_perm('taxi_management_app.add_vehicle'):
                raise PermissionDenied('Not allowed to add Vehicles.')
            vehicleSerializer = VehicleSerializer(data = request.data)
        vehicleSerializer.is_valid(raise_exception=True)
        vehicle = vehicleSerializer.save()
        taxiDriver.vehicle = vehicle
        taxiDriver.save()
        return Response({'user_id' : user.id, 'vehicle' : vehicleSerializer.data})

class TaxiTripStartView(APIView):
    """
    API Endpoint to start Taxi Trip.
    """
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        user = request.user
        try:
            taxiDriver = TaxiDriver.objects.get(user=user.id)
        except ObjectDoesNotExist:
            raise NotFound('User is not a Taxi Driver.')
        taxiTripSerializer = TaxiTripSerializer(data = {'taxi_driver': taxiDriver.id, 'start_timestamp': request.data.get('timestamp')})
        taxiTripSerializer.is_valid(raise_exception=True)
        trip = taxiTripSerializer.save()
        taxiTripPointSerializer = TaxiTripPointSerializer(data = {
            'taxi_trip' : trip.id,
            'lat' : request.data.get('lat'),
            'lng' : request.data.get('lng'),
            'timestamp' : request.data.get('timestamp'),
        })
        try:
            taxiTripPointSerializer.is_valid(raise_exception=True)
        except:
            # Delete trip if something goes wrong
            # TODO make transaction
            trip.delete()
            raise
        taxiTripPointSerializer.save()
        return Response(taxiTripPointSerializer.data)

class TaxiTripAddPointView(APIView):
    """
    API Endpoint to add TaxiTripPoint to Taxi Trip.
    """
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, trip_id):
        user = request.user
        try:
            taxiDriver = TaxiDriver.objects.get(user=user.id)
        except ObjectDoesNotExist:
            raise NotFound('User is not a Taxi Driver.')
        try:
            trip = TaxiTrip.objects.get(pk=trip_id)
        except ObjectDoesNotExist:
            raise NotFound('TaxiTrip not found.')
        if trip.taxi_driver.id != taxiDriver.id:
            raise PermissionDenied('User is not owner of TaxiTrip.')
        taxiTripPointSerializer = TaxiTripPointSerializer(data = {
            'taxi_trip' : trip.id,
            'lat' : request.data.get('lat'),
            'lng' : request.data.get('lng'),
            'timestamp' : request.data.get('timestamp'),
        })
        taxiTripPointSerializer.is_valid(raise_exception=True)
        taxiTripPointSerializer.save()
        return Response(taxiTripPointSerializer.data)

class TaxiTripEndView(APIView):
    """
    API Endpoint to end TaxiTrip.
    """
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, trip_id):
        user = request.user
        try:
            taxiDriver = TaxiDriver.objects.get(user=user.id)
        except ObjectDoesNotExist:
            raise NotFound('User is not a Taxi Driver.')
        try:
            trip = TaxiTrip.objects.get(pk=trip_id)
        except ObjectDoesNotExist:
            raise NotFound('TaxiTrip not found.')
        if trip.taxi_driver.id != taxiDriver.id:
            raise PermissionDenied('User is not the owner of TaxiTrip.')
        if trip.end_timestamp:
            raise serializers.ValidationError(detail='Trip has already ended')
        taxiTripPointSerializer = TaxiTripPointSerializer(data = {
            'taxi_trip' : trip.id,
            'lat' : request.data.get('lat'),
            'lng' : request.data.get('lng'),
            'timestamp' : request.data.get('timestamp'),
        })
        taxiTripPointSerializer.is_valid(raise_exception=True)
        taxiTripPointSerializer.save()
        taxiTripSerializer = TaxiTripSerializer(trip, data = {'end_timestamp': request.data.get('timestamp')}, partial=True)
        taxiTripSerializer.is_valid(raise_exception=True)
        taxiTripSerializer.save()
        return Response(taxiTripSerializer.data)
