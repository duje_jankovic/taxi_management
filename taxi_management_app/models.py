from datetime import date
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from . utils import getFormattedTimestamp

class TaxiCompany(models.Model):
    company_name = models.CharField(max_length=200)
    company_address = models.CharField(max_length=200)
    # Wage in € cents
    wage_per_km_driven = models.PositiveIntegerField(
        validators=[
            MinValueValidator(1)
        ]
    )
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Taxi Company"
        verbose_name_plural = "Taxi Companies"

    def __str__(self):
        return self.company_name

class Vehicle(models.Model):
    car_manufacturer = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    year_of_production = models.PositiveBigIntegerField(
        validators=[
            MinValueValidator(1900),
            MaxValueValidator(date.today().year)
        ]
    )
    license_plate = models.CharField(max_length=50, unique=True)

    class Meta:
        verbose_name = "Vehicle"
        verbose_name_plural = "Vehicles"

    def __str__(self):
        return self.car_manufacturer + ' ' + self.model + ' (' + str(self.year_of_production) + ') ' + self.license_plate

class TaxiDriver(models.Model):
    USER_GROUP = 'Taxi Drivers'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    taxi_company = models.ForeignKey(TaxiCompany, null=True, blank=True, on_delete=models.SET_NULL)
    vehicle = models.OneToOneField(Vehicle, null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Taxi Driver"
        verbose_name_plural = "Taxi Drivers"

    def __str__(self):
        return self.user.last_name + ' ' + self.user.first_name + ' (' + self.user.email + ')'

    def save(self, *args, **kwargs):
        try:
            taxiDriversGroup = Group.objects.get(name=self.USER_GROUP)
        except ObjectDoesNotExist:
            # TODO error logging and reporting via email or similar to admin users
            # Delete user if something went wrong
            raise
        self.user.groups.add(taxiDriversGroup)
        super().save(*args, **kwargs)

class TaxiTrip(models.Model):
    taxi_driver = models.ForeignKey(TaxiDriver, on_delete=models.CASCADE)
    start_timestamp = models.DateTimeField()
    end_timestamp = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Taxi Trip"
        verbose_name_plural = "Taxi Trips"

    def __str__(self):
        startTimestamp = getFormattedTimestamp(self.start_timestamp)
        endTimestamp = getFormattedTimestamp(self.end_timestamp) if self.end_timestamp else 'Not finished'
        return startTimestamp + ' - ' + endTimestamp + ' - ' + str(self.taxi_driver)

class TaxiTripPoint(models.Model):
    taxi_trip = models.ForeignKey(TaxiTrip, on_delete=models.CASCADE)
    lat = models.DecimalField(max_digits=9, decimal_places=6)
    lng = models.DecimalField(max_digits=9, decimal_places=6)
    timestamp = models.DateTimeField()

    class Meta:
        verbose_name = "Taxi Trip Point"
        verbose_name_plural = "Taxi Trip Points"

    def __str__(self):
        return getFormattedTimestamp(self.timestamp) + ', (' + str(self.lat) + ', ' + str(self.lat) + ')'
