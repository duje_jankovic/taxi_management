"""
Django production settings for taxi_management project.
"""
import os

# More info for production settings at https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['SECRET_KEY']

# SECURITY WARNING: update this when you have the production host
ALLOWED_HOSTS = ['0.0.0.0', 'localhost']