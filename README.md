
# Taxi Management App

## How to setup Taxi Management project for local development:
- create project directory
- position command line tool in created project folder
- clone repository:
	- run ```git clone https://gitlab.com/duje_jankovic/taxi_management.git```
- Install python requirements:
	- run```pip install -r requirements.txt```
- Initialize DB:
	- rename initial_db.sqlite3 to db.sqlite3
	- position command line tool to project root:
    - check for possible migrations:
	   - run ```python manage.py makemigrations```
	- run migrations (if there are any):
	   - run ```python manage.py migrate```
- Start server
	- position command line tool to project root:
	- run ```python manage.py runserver ```
## How to setup Taxi Management project for production development:
- create project directory
- position command line tool in created project folder
- clone repository:
	- run ```git clone https://gitlab.com/duje_jankovic/taxi_management.git```
- Install python requirements:
	- run```pip install -r requirements.txt```
- Initialize DB:
	- run ```python manage.py makemigrations```
	- run ```python manage.py migrate```
- create superuser
	- run  ```python manage.py createsuperuser```
- create user group **Supervisors**
	- add required permissions to the group
- create user group **Taxi Drivers**
	- add required  permissions to the group
- set production settings
	- add settings_production.py file in root/taxi_management/
	- write production settings (use settings_production_example.py for help)

## How to access administration:
### Admin location
- /admin (default : http://127.0.0.1:8000/admin)
### (DEV) Superuser credentials
- username: amodo
- password: 12345678
### (DEV) Taxi company admin credentials
- username: supervisior1
- password: super123!
- **NOTE:**
	- requested superuser is actually normal user assigned to group **Supervisors**
### How to create Supervisor user
- login as **Superuser**
- create new user (Supervisor register API is under construction)
- set user **Staff status** to **True**
- assign user to group **Supervisors**
- make user change his password (Supervisor register API is under construction)
## How to use API:
- Swagger documentation - coming soon :)
### Taxi Driver Register
- location: /api/taxi_drivers/register/
- method: **POST**
- request data:
	-  email
	-  password
	-  first_name
	-  last_name
- example (using httpie):
 ```http post http://127.0.0.1:8000/api/taxi_drivers/register/ email="example_mail@gmail.com" password="example_password" first_name="John" last_name="Doe" ```
- response data:
	```
			"email": "example_mail@gmail.com",
	        "first_name": "John",
	        "id": 5,
	        "last_name": "Doe",
	        "username": "example_mail@gmail.com"
	```

### User Login
- location: /api/login/
- method: **POST**
- request data:
	-  username
	-  password
- example (using httpie):
	 ```http post http://127.0.0.1:8000/api/login/ username="example_mail@gmail.com" password="example_password" ```
 - response data:
	```{"token": "user_token"}```
### Get (active) Taxi Companies List
- location: /api/taxi_companies/
- method: **GET**
- request data:
	- Header data
		-  Authorization : user_token
- example (using httpie):
	 ```http get http://127.0.0.1:8000/api/taxi_companies/ "Authorization: Token user_token" ```
 - response data:
	```
	[
	    {
        "active": true,
        "company_address": "Vukovarska 101",
        "company_name": "Uber",
        "id": 1,
        "wage_per_km_driven": 123
	    },
	    ...
    ]
    ```
### Set Taxi Driver company
- location: /taxi_drivers/user_id/set_company/
- method: **PATCH**
- request data:
	- Header data
		-  Authorization : user_token
	-  taxi_company_id
- example (using httpie):
	 ```http patch http://127.0.0.1:8000/api/taxi_drivers/5/set_company/ taxi_company_id="1" "Authorization: Token user_token" ```
 - response data:
	```
	{
	    "taxi_company_id": 1,
	    "user_id": 5
	}
    ```
### Set Taxi Driver vehicle
- location: /taxi_drivers/user_id/set_company/
- method: **PATCH**
- request data:
	- Header data
		-  Authorization : user_token
	-  car_manufacturer
	-  model
	-  year_of_production
	-  license_plate
- example (using httpie):
	 ```http patch http://127.0.0.1:8000/api/taxi_drivers/5/set_vehicle/ car_manufacturer="Toyota" model="Yaris" year_of_production="2019" license_plate="ST-1234-DJ" "Authorization: Token user_token" ```
 - response data:
	```
	{
	    "user_id": 5,
	    "vehicle": {
	        "car_manufacturer": "Toyota",
	        "license_plate": "ST-1234-DJ",
	        "model": "Yaris",
	        "year_of_production": 2019
	    }
	}
    ```
### Start Taxi Trip
- location: /taxi_drivers/taxi_trips/start_trip/
- method: **POST**
- request data:
	- Header data
		-  Authorization : user_token
	-  timestamp
	-  lat
	-  lng
- example (using httpie):
	 ```http post http://127.0.0.1:8000/api/taxi_trips/start_trip/ timestamp="2021-07-28T18:46:10+02:00" lat="43.508133" lng="16.440193" "Authorization: Token user_token" ```
 - response data:
	```
	{
	    "lat": "43.508133",
	    "lng": "16.440193",
	    "taxi_trip": 4,
	    "timestamp": "2021-07-28T18:46:10+02:00"
	}
    ```
### Add Taxi Trip Point
- location: /taxi_drivers/taxi_trips/trip_id/add_point/
- method: **POST**
- request data:
	- Header data
		-  Authorization : user_token
	-  timestamp
	-  lat
	-  lng
- example (using httpie):
	 ```http post http://127.0.0.1:8000/api/taxi_trips/4/add_point/ timestamp="2021-07-28T18:46:10+02:00" lat="43.508133" lng="16.440193" "Authorization: Token user_token" ```
 - response data:
	```
	{
	    "lat": "43.508133",
	    "lng": "16.440193",
	    "taxi_trip": 4,
	    "timestamp": "2021-07-28T18:46:10+02:00"
	}
    ```
 ### Add Taxi Trip Point
- location: /taxi_drivers/taxi_trips/trip_id/end_trip/
- method: **POST**
- request data:
	- Header data
		-  Authorization : user_token
	-  timestamp
	-  lat
	-  lng
- example (using httpie):
	 ```http post http://127.0.0.1:8000/api/taxi_trips/4/end_trip/ timestamp="2021-07-28T18:46:10+02:00" lat="43.508133" lng="16.440193" "Authorization: Token user_token" ```
 - response data:
	```
	{
	    "end_timestamp": "2021-07-28T18:46:10+02:00",
	    "id": 4,
	    "start_timestamp": "2021-07-28T18:46:10+02:00",
	    "taxi_driver": 3
	}
    ```